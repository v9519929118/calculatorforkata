import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) throws Exception {
        String str = requestString();
        System.out.println(calculator(str));


    }

    public static String calculator(String example) throws Exception {
        String[] splitTest = example.split(" ");
        String finalResult = "";

        if (splitTest.length > 3) {
            throw new Exception("throws Exception //т.к. формат математической операции не удовлетворяет заданию - два операнда и один оператор (+, -, /, *)");
        } else if (splitTest.length < 3) {
            throw new Exception("throws Exception //т.к. строка не является математической операцией");
        } else {
            String[] arabArr = {"1", "2", "3", "4", "5", "6", "7", "9", "9", "10"};
            String[] rimArr = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};
            int firstNumber = 0;
            int secondNumber = 0;
            int result = 0;
            boolean firstNumberIsRim = false;
            boolean secondNumberIsRim = false;

            for (int i = 0; i < arabArr.length; i++) {
                if (splitTest[0].equals(arabArr[i])) {
                    firstNumber = Integer.parseInt(arabArr[i]);
                    break;
                } else if (splitTest[0].equals(rimArr[i])) {
                    firstNumber = Integer.parseInt(arabArr[i]);
                    firstNumberIsRim = true;
                    break;
                } else if (i == 9) {
                    throw new Exception("Вводные данные не подходят под заданные параметры");
                }
            }

            for (int i = 0; i < arabArr.length; i++) {
                if (splitTest[2].equals(arabArr[i])) {
                    secondNumber = Integer.parseInt(arabArr[i]);
                    break;
                } else if (splitTest[2].equals(rimArr[i])) {
                    secondNumber = Integer.parseInt(arabArr[i]);
                    secondNumberIsRim = true;
                    break;
                } else if (i == 9) {
                    throw new Exception("Вводные данные не подходят под заданные параметры");
                }
            }
            if (firstNumberIsRim == secondNumberIsRim) {
                switch (splitTest[1]) {
                    case "+":
                        result = firstNumber + secondNumber;
                        break;
                    case "-":
                        result = firstNumber - secondNumber;
                        break;
                    case "*":
                        result = firstNumber * secondNumber;
                        break;
                    case "/":
                        result = firstNumber / secondNumber;
                        break;
                    default:
                        throw new Exception("Использован неверный знак для вычисления примера");
                }
                if (firstNumberIsRim & secondNumberIsRim) {
                    if (result < 0) {
                        throw new Exception("throws Exception //т.к. в римской системе нет отрицательных чисел");
                    } else {
                        finalResult = rimResult(result, rimArr, arabArr);
                    }
                } else
                    finalResult = (Integer.toString(result));
            } else
                throw new Exception("throws Exception //т.к. используются одновременно разные системы счисления");
        }
        return finalResult;
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите ваш пример: ");
        return scanner.nextLine();
    }

    public static String rimResult(int res, String[] rimArray, String[] arabArray) throws Exception {
        String rimRes = "";

        if (res < 0) {
            throw new Exception("throws Exception //т.к. в римской системе нет отрицательных чисел");
        } else if (res <= 10) {
            rimRes = (Integer.toString(res));
            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    rimRes = rimArray[i];
                }
            }
        } else if (res > 10 & res <= 20) {
            int secondPartCount;
            secondPartCount = res - 10;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("X").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 20 & res <= 30) {
            int secondPartCount;
            secondPartCount = res - 20;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("XX").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 30 & res <= 40) {
            int secondPartCount;
            secondPartCount = res - 30;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("XXX").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 40 & res < 50) {
            int secondPartCount;
            secondPartCount = res - 40;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("XL").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res == 50 ) {
            rimRes = "L";
        }
        else if (res > 50 & res <= 60) {
            int secondPartCount;
            secondPartCount = res - 50;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("L").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 60 & res <= 70) {
            int secondPartCount;
            secondPartCount = res - 60;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("LX").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 70 & res <= 80) {
            int secondPartCount;
            secondPartCount = res - 70;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("LXX").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 80 & res <= 90) {
            int secondPartCount;
            secondPartCount = res - 10;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("LXXX").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else if (res > 90 & res < 100) {
            int secondPartCount;
            secondPartCount = res - 10;
            rimRes = Integer.toString(secondPartCount);
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arabArray.length; i++) {
                if (rimRes.equals(arabArray[i])) {
                    stringBuilder.append("XC").append(rimArray[i]);
                    rimRes = stringBuilder.toString();
                }
            }
        } else {
            rimRes = "C";
        }
        return rimRes;
    }
}

